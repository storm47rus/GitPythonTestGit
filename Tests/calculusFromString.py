import re
def calc(expression):
    original_expression = expression
    expression = ""
    original_expression = original_expression.replace(" ","")
    original_expression = original_expression.replace("--","+")
    original_expression = original_expression.replace("+-","-")
    original_expression = original_expression.replace("-+","-")
    while 1:
        re_findall = re.findall(r"\(\-?\d+\)", original_expression)
        print(re_findall)
        if re_findall:
            original_expression = original_expression.replace(re_findall[0], re_findall[0][1:-1])
            print(original_expression)
        else:
            break
        
    i = 0

    result = 0
    action = 0
    number = 0
    result = 0


    while original_expression.find("(") + 1 or original_expression.find(")") + 1:
        expression = original_expression
        bracket_start = expression.find("(")
        bracket_end = expression.rfind(")")
        while bracket_start != -1 or bracket_end != -1:
            print(expression)
            expression = expression[bracket_start + 1:bracket_end]
            bracket_start = expression.find("(")
            bracket_end = expression.rfind(")")
            print("/////////////")

        print(expression)
        while 1:
            re_findall = re.findall(r"\(\-?\d+\)", original_expression)
            print(re_findall)
            if re_findall:
                original_expression = original_expression.replace(re_findall[0], re_findall[0][1:-1])
                print(original_expression)
            else:
                break
        expression = expression.replace("--","+")
        expression = expression.replace("+-","-")
        expression = expression.replace("-+","-")
        original_expression = original_expression.replace("--","+")
        original_expression = original_expression.replace("+-","-")
        original_expression = original_expression.replace("-+","-")
        print(expression)

        print("\ncalculus!\n")

        backup_expression = expression
        print(expression)
        if "*" not in expression and "/" not in expression:
            actions = re.split("[\d.?]+", expression)[1:-1]
            expression = re.split("[+\-]", expression)
        else:
            actions = re.split("\w?\-?[\d.?]+", expression)[1:-1]
            expression = re.split("[+\/\*]", expression)
        print(actions)
        print(expression)
        for i in range(0, len(actions)):
            if actions[i] == "":
                actions[i] = "+"
        print(actions)
        expression = calculus_maximus(actions, expression)

        print()
        print("было:", original_expression)
        original_expression = original_expression.replace("(" + backup_expression + ")", str(expression[0]))
        print("стало:", original_expression)
        i += 1
        if i > 8:
            break

        print("\nend of brackets\n----------------------------\n")

    original_expression = original_expression.replace("--","+")
    original_expression = original_expression.replace("+-","-")
    original_expression = original_expression.replace("-+","-")
    
    print(original_expression)

    print("\ncalculus!\n")

    backup_expression = original_expression
    if "*" not in original_expression and "/" not in original_expression:
        actions = re.split("[\d.?]+", original_expression)[1:-1]
        original_expression = re.split("[+\-]", original_expression)
    else:
        actions = re.split("\w?\-?[\d.?]+", original_expression)[1:-1]
        original_expression = re.split("[+\/\*]", original_expression)
    for i in range(0, len(actions)):
        if actions[i] == "":
            actions[i] = "+"
    print(actions)
    original_expression = calculus_maximus(actions, original_expression)
    
    print(f"Результат: {original_expression[0]}")
    if type(original_expression[0]) == str:
        if "." in original_expression[0]:
            return float(original_expression[0])
        else:
            return int(original_expression[0])
    else:
        return original_expression[0]


def calculus_maximus(actions: list, expression: list):
    while 1:
        print("calculus_maximus")
        print(actions)
        print(expression)
        if "/" in actions:
            print("found /:")
            sign_index = actions.index("/")
            expression[sign_index] = float(
                expression[sign_index]) / float(expression[sign_index + 1])
            del expression[sign_index + 1]
            actions.remove("/")
            print(actions)
            print(expression)
        elif "*" in actions:
            print("found *:")
            sign_index = actions.index("*")
            expression[sign_index] = float(
                expression[sign_index]) * float(expression[sign_index + 1])
            del expression[sign_index + 1]
            actions.remove("*")
            print(actions)
            print(expression)
        elif "-" in actions:
            print("found -:")
            sign_index = actions.index("-")
            expression[sign_index] = float(
                expression[sign_index]) - float(expression[sign_index + 1])
            del expression[sign_index + 1]
            actions.remove("-")
            print(actions)
            print(expression)
        elif "+" in actions:
            print("found +:")
            sign_index = actions.index("+")
            expression[sign_index] = float(
                expression[sign_index]) + float(expression[sign_index + 1])
            del expression[sign_index + 1]
            actions.remove("+")
            print(actions)
            print(expression)
        else:
            print("\nEnd of the calculus_maximus")
            print(expression)
            break
        print()
    return expression

